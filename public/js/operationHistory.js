// class that contains all data and methods for history manipulation
export default class HistoryStack{
    constructor(){
        this.undoCanvasStack = [];
        this.redoCanvasStack = [];
    }

    addToStack = (f) => {
        let canvas = document.querySelector('#canvas');
        let ctx = canvas.getContext('2d');
        this.undoCanvasStack.push([f.copy(), ctx.getImageData(0,0,canvas.width, canvas.height)]);
        this.redoCanvasStack = [];

    }

    undo = (f) => {
        if(this.undoCanvasStack.length == 0) return f;
        let canvas = document.querySelector('#canvas');
        let ctx = canvas.getContext('2d');
        let currCanvas = this.undoCanvasStack.pop();
        this.redoCanvasStack.push([f.copy(), ctx.getImageData(0,0,canvas.width, canvas.height)]);
        ctx.putImageData(currCanvas[1], 0, 0);
        return currCanvas[0];
    }

    redo = (f) => {
        if(this.redoCanvasStack.length == 0) return f;
        let canvas = document.querySelector('#canvas');
        let ctx = canvas.getContext('2d');
        let nextCanvas = this.redoCanvasStack.pop();
        this.undoCanvasStack.push([f.copy(), ctx.getImageData(0,0,canvas.width, canvas.height)]);
        ctx.putImageData(nextCanvas[1], 0, 0);
        return nextCanvas[0];
    }

    clearHistory = () => {
        this.undoCanvasStack = [];
        this.redoCanvasStack = [];
    }

    removeFromStack = () => {
        this.undoCanvasStack.pop();
    }
}