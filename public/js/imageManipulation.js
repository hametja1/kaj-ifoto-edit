// class that holds setup of the base sliders values
export default class FiltersSetup{
    constructor(){
        this.exposureCoef = 0.0;
        this.brightnessCoef = 0.0;
        this.contrastCoef = 0.0;
        this.saturationCoef = 0.0;
        this.hueCoef = 0.0;
    }

    set exposure(val){
        this.exposureCoef = parseFloat(val);
    }

    set brightness(val){
        this.brightnessCoef = 255*parseFloat(val);
    }

    set contrast(val){
        this.contrastCoef = Math.pow((parseFloat(val)+1), 2);
    }

    set saturation(val){
        this.saturationCoef = parseFloat(val)*(-1);
    }

    set hue(val){
        this.hueCoef = parseFloat(val)*10;
    }

    get exposure(){
        return this.exposureCoef;
    }

    get brightness(){
        return this.brightnessCoef/255;
    }
    get contrast(){
        if(this.contrastCoef == 0.0) return 0.0;
        return Math.sqrt(this.contrastCoef)-1;
    }
    get saturation(){
        return this.saturationCoef*(-1);
    }
    get hue(){
        return this.hueCoef/10;
    }

    copy(){
        let f = new FiltersSetup();
        f.exposureCoef = this.exposureCoef;
        f.brightnessCoef = this.brightnessCoef;
        f.contrastCoef = this.contrastCoef;
        f.saturationCoef = this.saturationCoef;
        f.hueCoef = this.hueCoef;
        return f;
    }

    AddExposure(rgba, bezier, chans){
        let _k, _ref2;
        for (let i = _k = 0, _ref2 = chans.length; 0 <= _ref2 ? _k < _ref2 : _k > _ref2; i = 0 <= _ref2 ? ++_k : --_k) {
          rgba[chans[i]] = bezier[rgba[chans[i]]];
        }
        return rgba;
    }

    AddBrightness(rgba){
        rgba.r += this.brightnessCoef;
        rgba.g += this.brightnessCoef;
        rgba.b += this.brightnessCoef;
        return rgba;
    }

    AddContrast(rgba){
        rgba.r /= 255;
        rgba.r -= 0.5;
        rgba.r *= this.contrastCoef;
        rgba.r += 0.5;
        rgba.r *= 255;
        rgba.g /= 255;
        rgba.g -= 0.5;
        rgba.g *= this.contrastCoef;
        rgba.g += 0.5;
        rgba.g *= 255;
        rgba.b /= 255;
        rgba.b -= 0.5;
        rgba.b *= this.contrastCoef;
        rgba.b += 0.5;
        rgba.b *= 255;
        return rgba;
    }

    AddSaturation(rgba){
        var max;
        max = Math.max(rgba.r, rgba.g, rgba.b);
        if (rgba.r !== max) {
          rgba.r += (max - rgba.r) * this.saturationCoef;
        }
        if (rgba.g !== max) {
          rgba.g += (max - rgba.g) * this.saturationCoef;
        }
        if (rgba.b !== max) {
          rgba.b += (max - rgba.b) * this.saturationCoef;
        }
        return rgba;
    }

    AddHue(rgba){
        var b, g, h, hsv, r, _ref;
        hsv = Caman.Convert.rgbToHSV(rgba.r, rgba.g, rgba.b);
        h = hsv.h * 100;
        h += Math.abs(this.hueCoef);
        h = h % 100;
        h /= 100;
        hsv.h = h;
        _ref = Caman.Convert.hsvToRGB(hsv.h, hsv.s, hsv.v), r = _ref.r, g = _ref.g, b = _ref.b;
        rgba.r = r;
        rgba.g = g;
        rgba.b = b;
        return rgba;
    }

}

