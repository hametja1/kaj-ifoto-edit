import FiltersSetup from './imageManipulation.js';
import HistoryStack from './operationHistory.js';

let img = new Image();
let f = new FiltersSetup();
let history = new HistoryStack();
let imgLoaded = false;
let fileName = '';
let mDSliderValue;

Caman.DEBUG = ('console' in window);

const canvas = document.createElement('canvas');
const ctx = canvas.getContext('2d');

const uploadFile = document.querySelector('#input-file');
const canvasParent = document.querySelector('div.canvas-field');
const baseSliderParent = document.querySelector('#filters');
const filterButtons = document.querySelectorAll('.filter-button');
const resetButton = document.querySelector('#reset');
const downloadButton = document.querySelector('#download');
const dropdown1 = document.querySelector('#dropdown1');
const dropdown2 = document.querySelector('#dropdown2');
const loadingAnim = document.querySelector('#loadingLabel');
const undoButt = document.querySelector('#undo');
const redoButt = document.querySelector('#redo');
const sliders = document.querySelectorAll('input.slider');
const dropArea = document.querySelector('div.canvas-field');

// Listener for arrow icon class change in dropdown menu for base filters
dropdown1.addEventListener('input', (e) => {
    if(e.target.checked){
        document.querySelector('#dropdown1arrow').classList.add('img-rotate');
    }else{
        document.querySelector('#dropdown1arrow').classList.remove('img-rotate');
    }
});

// Listener for arrow icon class change in dropdown menu for filter presets
dropdown2.addEventListener('input', (e) => {
    if(e.target.checked){
        document.querySelector('#dropdown2arrow').classList.add('img-rotate');
    }else{
        document.querySelector('#dropdown2arrow').classList.remove('img-rotate');
    }
});

// Listener for control base filter sliders
baseSliderParent.addEventListener('input', (e) => {
    if(imgLoaded){
        switch(e.target.id){
            case 'exposure':
                f.exposure = e.target.value;
                break;        
            case 'brightness':
                f.brightness = e.target.value;
                break;
            case 'contrast':
                f.contrast = e.target.value;
                break;
            case 'saturation':
                f.saturation = e.target.value;
                break;
            case 'hue':
                f.hue = e.target.value;
                break;
        }
        Caman('#canvas', img, function(){
            this.baseFilters(f);
            this.render();
        });
        
        document.querySelector(`#in-${e.target.id}`).textContent = e.target.value;
    }else{
        e.target.value = 0;
    }

});

// Listener for controll all filter presets buttons
filterButtons.forEach(button => {
    button.addEventListener('click', e => filterButtonClick(e), true);
});

// Listener for save current value of the slider, when it is dragged
sliders.forEach(slider => slider.addEventListener('mousedown', (e) => {
    history.addToStack(f);
    mDSliderValue = e.target.value;
}));

// Listener for check value change of dragged slider when mouse release.
// If the value changed, then remove last saved history data (= dont save history if it is without change).
sliders.forEach(slider => slider.addEventListener('mouseup', (e) => {
    if(mDSliderValue == e.target.value) history.removeFromStack();
}));

// Function for determine, which preset filter button was pressed. Then aply corespond filter.
let filterButtonClick = (e) => {
    if(imgLoaded){
        history.addToStack(f);
        switch(e.target.id){
            case 'vintage':
                Caman('#canvas', img, function(){
                    this.vintage();
                    this.render();
                });
                break;        
            case 'hemingway':
                Caman('#canvas', img, function(){
                    this.hemingway();
                    this.render();
                });
                break;
            case 'clarity':
                Caman('#canvas', img, function(){
                    this.clarity();
                    this.render();
                });
                break;
            case 'sincity':
                Caman('#canvas', img, function(){
                    this.sinCity();
                    this.render();
                });
                break;
            case 'sunrise':
                Caman('#canvas', img, function(){
                    this.sunrise();
                    this.render();
                });
                break;
            case 'crosprocess':
                Caman('#canvas', img, function(){
                    this.crossProcess();
                    this.render();
                });
                break;
            case 'orangepeel':
                Caman('#canvas', img, function(){
                    this.orangePeel();
                    this.render();
                });
                break;
            case 'love':
                Caman('#canvas', img, function(){
                    this.love();
                    this.render();
                });
                break;
            case 'grungy':
                Caman('#canvas', img, function(){
                    this.grungy();
                    this.render();
                });
                break;
            case 'jarques':
                Caman('#canvas', img, function(){
                    this.jarques();
                    this.render();
                });
                break;
            case 'pinhole':
                Caman('#canvas', img, function(){
                    this.pinhole();
                    this.render();
                });
                break;
            case 'oldboot':
                Caman('#canvas', img, function(){
                    this.oldBoot();
                    this.render();
                });
                break;
            case 'glowingsun':
                Caman('#canvas', img, function(){
                    this.glowingSun();
                    this.render();
                });
                break;
            case 'hazydays':
                Caman('#canvas', img, function(){
                    this.hazyDays();
                    this.render();
                });
                break;
            case 'hermajesty':
                Caman('#canvas', img, function(){
                    this.herMajesty();
                    this.render();
                });
                break;
            case 'nostalgia':
                Caman('#canvas', img, function(){
                    this.nostalgia();
                    this.render();
                });
                break;
            case 'concentrate':
                Caman('#canvas', img, function(){
                    this.concentrate();
                    this.render();
                });
                break;   
        }
 
    }
}

// Function for sliders reset
let resetSliders = () => {
    sliders[0].value = f.exposure;
    sliders[1].value = f.brightness;
    sliders[2].value = f.contrast;
    sliders[3].value = f.saturation;
    sliders[4].value = f.hue;
    let slidersVal = document.querySelectorAll('label.slider-val');
    slidersVal[0].textContent = f.exposure.toString();
    slidersVal[1].textContent = f.brightness.toString();
    slidersVal[2].textContent = f.contrast.toString();
    slidersVal[3].textContent = f.saturation.toString();
    slidersVal[4].textContent = f.hue.toString();
}

// Listener for reset button. When pressed, resets the image modifications and sliders values.
resetButton.addEventListener('click', (e) => {
    f = new FiltersSetup();
    resetSliders();
    history.clearHistory();
    if(imgLoaded){
        Caman('#canvas', img, function(){
            this.revert(false);
            this.render();
        });    
    }

});

// Function for download current content of the canvas
let download = (canv, fileName) => {
    let e;
    const l = document.createElement('a');
    l.download = fileName;
    l.href = canvas.toDataURL('image/jpeg', 0.8);
    e = new MouseEvent('click');
    l.dispatchEvent(e);
}

// Listener for download button
downloadButton.addEventListener('click', (e) => {
    if(imgLoaded){
        const fileExtension = fileName.slice(-4);
        let newFileName;
        if(fileExtension === '.jpg' || fileExtension === '.png'){
            newFileName = fileName.substring(0, fileName.length - 4) + '_new.jpg';
        }
        download(canvas, newFileName);
    }
});

// Drag and drop section ---------------------------------------

let highlightDrop = (e) => {
    dropArea.classList.add('drop-highlight');
};

let unHighlightDrop = (e) => {
    dropArea.classList.remove('drop-highlight');
};

let prevDefaults = (e) => {
    e.preventDefault();
    e.stopPropagation();
};

let handleDrop = (e) => {
    let dataTr = e.dataTransfer;
    let files = dataTr.files;
    loadFile(files[0]);
}

['dragenter', 'dragover', 'dragleave', 'drop'].forEach(e => {
    dropArea.addEventListener(e, prevDefaults, false)
});

['dragenter', 'dragover'].forEach(e => {
    dropArea.addEventListener(e, highlightDrop, false);
});

['dragleave', 'drop'].forEach(e => {
    dropArea.addEventListener(e, unHighlightDrop, false);
});

dropArea.addEventListener('drop', handleDrop, false);

// Drag and drop section end ---------------------------------------

// Function that is used to loads the Image file
let loadFile = (file) => {
    const reader = new FileReader();

    if(file){
        fileName = file.name;
        reader.readAsDataURL(file);
    }

    reader.addEventListener('load', () => {
        img = new Image();

        img.src = reader.result;

        img.onload = () => {
            canvas.width = img.width;
            canvas.height = img.height;
            canvas.id = 'canvas';
            ctx.drawImage(img, 0, 0, img.width, img.height);
            canvas.removeAttribute('data-caman-id');
            while(canvasParent.firstChild){
                canvasParent.firstChild.remove();
            }
            canvasParent.appendChild(canvas);
            canvasParent.classList.remove('canvas-field-h');

            imgLoaded = true;
        }
    }, false); 
};

// Listener for image file input
uploadFile.addEventListener('change', (e) =>{
    const file = document.querySelector('#input-file').files[0];

    loadFile(file);
});

// Listener for undo button click
undoButt.addEventListener('click', (e) => {
    f = history.undo(f);
    resetSliders();
});

// Listener for redo button click
redoButt.addEventListener('click', e => {
    f = history.redo(f);
    resetSliders();
});

// Listener for change history position with Ctrl+Z and Ctrl+Y
document.addEventListener('keydown', e => {
    console.log(e);
    if(e.ctrlKey && e.key == 'z'){
        f = history.undo(f);
        resetSliders();
    }
    else if(e.ctrlKey && e.key == 'y'){
        f = history.redo(f);
        resetSliders();
    }
})

// Display process animation
Caman.Event.listen("processStart", (job) => {
    loadingAnim.style.display = "flex";
});

// Hide process animation
Caman.Event.listen("processComplete", (job) => {
    loadingAnim.style.display = "none";
});

// Listener for button labels hiding
window.addEventListener('resize', e => {
    if(e.currentTarget.innerWidth <= 750){
        redoButt.textContent = undoButt.textContent = resetButton.textContent = downloadButton.textContent = '';
    }
    else{
        redoButt.textContent = 'Redo';
        undoButt.textContent = 'Undo';
        resetButton.textContent = 'Reset';
        downloadButton.textContent = 'Download';
    }
})