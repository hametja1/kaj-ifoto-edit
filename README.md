IFoto Editor - semester work for the KAJ subject

This application serves as a basic image editor with the ability to edit basic parameters and apply more advanced presets.
Images are inserted into the application from the user's disk either via the file input field, by clicking on the main canvas, or by drag&drop method.
All available image editing filters are then located in the left side menu.
The application has also implemented an edit history, which can be navigated using the Undo and Redo buttons, or the Ctrl+Z/Ctrl+Y keyboard shortcuts.

The image can then be downloaded to your computer using the Download button.
